import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-material-page',
  templateUrl: './material-page.component.html',
  styleUrls: ['./material-page.component.sass'],
})
export class MaterialPageComponent implements OnInit {
  // radio button
  favoriteSeason: string;
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
  // slider
  checked = true
  disabled = false

  constructor() {}

  ngOnInit() {}
}
