import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialPageComponent } from './components/material-page/material-page.component';

const routes: Routes = [
  { path: '',  component: MaterialPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialRoutingModule { }
